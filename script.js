/*************Variables Globales**************/
let partie;
/*********************************************/

/**********Function d'initialisation***********/
$(document).ready(function() {
    $('#drapeau div').click(change_color);
    $("#joker").click(useJoker);
    $('#current_level').text('0');
    $('#nb_levels').text(FLAGS.length);
    $("#boutonStart").click(start);
    $("#abandon").click(skip);
    $('.bubble').hide();
    $("#bouttonRegles").click(afficherRegles);
    $('#affichageScore').slideUp();
    $('#zoneGrise').slideUp();
    $('#boutonRestart').click(restartGame);
    window.onunload = savePartie;
    setPartie();
});
/********************************************* */


function afficherRegles() {
    $('.bubble').toggle();
}

function savePartie() {
    if (partie.end) {
        localStorage.removeItem('partie');
    } else {
        localStorage.setItem('partie', JSON.stringify(partie));
    }

}

function setPartie() {
    partie = JSON.parse(localStorage.getItem('partie'));
    console.log(partie);
    if (partie == null || partie.level == -1) {
        partie = {
            end: false,
            left_color: -1,
            right_color: -1,
            center_color: -1,
            level: -1,
            compteur: 0,
            tempsTotal: 0,
            compteurTotal: 0,
            score: 0,
            score_flag: 0,
            timer: {
                sec: 0,
                min: 0,
                h: 0
            },
            joker: false
        }
        $('#menuStart').css('visibility', 'visible');
    } else {
        resetCSSFlag();
        setCountry();
        $("#timer").html(partie.timer.h + " : " + partie.timer.min + " : " + partie.timer.sec);
        startTimer();
        $('#compteurChiffre').html(partie.compteur);
        $('#left').css('backgroundColor', FLAGS[partie.level].colors[partie.left_color]);
        $('#right').css('backgroundColor', FLAGS[partie.level].colors[partie.right_color]);
        if (partie.center_color > -1) {
            $('#center').css('backgroundColor', FLAGS[partie.level].colors[partie.center_color]);
        }
        if (partie.joker) {
            useJoker();
        }
    }
}



/***********************FUNCTIONS************************/

function change_color(event) {
    switch (event.target.id) {
        case 'left':
            partie.left_color = nextColor(partie.left_color, FLAGS[partie.level].colors);
            $('#left').css('backgroundColor', FLAGS[partie.level].colors[partie.left_color]);

            break;
        case 'center':
            partie.center_color = nextColor(partie.center_color, FLAGS[partie.level].colors);
            $('#center').css('backgroundColor', FLAGS[partie.level].colors[partie.center_color]);
            break;
        case 'right':
            partie.right_color = nextColor(partie.right_color, FLAGS[partie.level].colors);
            $('#right').css('backgroundColor', FLAGS[partie.level].colors[partie.right_color]);
            break;
        default:
            break;
    }
    counter();
    verif();
}

function nextColor(color, colors_tab) {
    let res = color + 1;
    return (res < colors_tab.length) ? res : 0;
}

/***********************************************************/

/*******************COMPTEUR CLIC**************************** */

function counter() {
    partie.compteur = partie.compteur + 1;
    $('#compteurChiffre').html(partie.compteur);
}

function resetCounter() {
    partie.compteur = 0;
    $('#compteurChiffre').text('0');
}

/*******************FONCTION DE VERIFICATION**************************** */


function verif() {
    let colors_tab = FLAGS[partie.level].colors;
    if (partie.left_color == 0 &&
        (
            (colors_tab.length == 2 && partie.right_color == 1) ||
            (colors_tab.length == 3 && partie.center_color == 1 && partie.right_color == 2)
        )) {
        partie.tempsTotal += getTempEnSecond();
        getScore();
        partie.compteurTotal += partie.compteur;
        affichage(1);
        if (partie.level < FLAGS.length - 1) {
            setTimeout(nextLevel, 2000);
        } else {
            partie.end = true;
            setTimeout(popUpFinal, 2000);
        }
    } else {
        console.log("retente ta chance !");
    }
};

/*******************FONCTION DE RESET**************************** */
function reset() {
    resetCSSFlag();
    resetTimer();
    resetCounter();
    resetJoker();
    partie.score_flag = 0;
    partie.left_color = -1;
    partie.center_color = -1;
    partie.right_color = -1;
}


/*******************FONCTION PAYS**************************** */
function traitement() {


}

function nextLevel() {
    partie.level += 1;
    reset();
    setCountry();
}

function setCountry() {
    $('#current_level').text(partie.level + 1);
    $('#pays').text(FLAGS[partie.level].name);
    $('#div-pays').css('width', (FLAGS[partie.level].name.length * 7) + "%");
    setCssClassFlag();
}

function setCssClassFlag() {
    switch (FLAGS[partie.level].type) {
        case 'horizontal_3':
            $('#drapeau').css({ 'display': 'flex', 'flex-direction': 'column' });
            $('#drapeau div').addClass('horizontal_3');
            break;
        case 'horizontal_2':
            $('#center').hide();
            $('#drapeau').css({ 'display': 'flex', 'flex-direction': 'column' });
            $('#drapeau div').addClass('horizontal_2');
            break;
        case 'vertical_3':
            $('#drapeau').css({ 'display': 'flex', 'flex-direction': 'row' });
            $('#drapeau div').addClass('vertical_3');
            break;
        case 'triangle':
            $('#left').addClass('triangle_l');
            $('#right').addClass('triangle_r');
            $('#center').addClass('triangle_center');
            $('#drapeau').css({ 'display': 'grid', 'grid-template': 'repeat(8, 1fr) / repeat(12, 1fr)' });
            break;
        default:
            break;
    }
}

function resetCSSFlag() {
    $('#drapeau div')
        .removeClass('horizontal_3')
        .removeClass('horizontal_2')
        .removeClass('vertical_3')
        .removeClass('triangle_center')
        .removeClass('triangle_l')
        .removeClass('triangle_r');
    $('#center').show();
    $('#drapeau div').css('background-color', 'rgb(245, 246, 238, 0.50)');
    $('#drapeau').css('grid-template ', ' none');
}
/*******************TIMER**************************** */

function startTimer() {
    setInterval(function() {
        $("#timer").html(partie.timer.h + " : " + partie.timer.min + " : " + partie.timer.sec);
        if (partie.timer.sec < 59) {
            partie.timer.sec += 1;
        } else if (partie.timer.min < 59) {
            partie.timer.min += 1
            partie.timer.sec = 0;
        } else {
            partie.timer.h += 1
            partie.timer.min = 0;
        }
    }, 1000);
}

function resetTimer() {
    partie.timer.sec = 0;
    partie.timer.min = 0;
    partie.timer.h = 0;
}

/*******************FINAL**************************** */
function popUpFinal() {
    $("#nbrClicks").html(partie.compteurTotal);
    $("#nbrTps").html(partie.tempsTotal + ' secondes');
    $("#nbrPts").html(partie.score + ' points');
    $('#menuFin').css('visibility', 'visible');

}

/*******************START**************************** */


function start() {
    $('#menuStart').css('visibility', 'hidden');
    startTimer();
    nextLevel();
};
/*******************JOKER**************************** */


function useJoker() {
    $('#joker').attr('disabled', true);
    partie.joker = true;
    partie.left_color = 0;
    $('#left').css({
        'background-color': FLAGS[partie.level].colors[0],
        'pointer-events': 'none'
    });
    verif();
}

function resetJoker() {
    $('#joker').attr('disabled', false);
    partie.joker = false;
    $('#left').css('pointer-events', 'all');
}

/*******************AFFICHAGE INTERMEDIAIRE**************************** */
function affichage(type) {
    $('#affichageScore').css('visibility', 'visible');
    $('#zoneGrise').css('visibility', 'visible');
    switch (type) {
        case 1:
            $('#abandon_pop').hide();
            $('#result_pop').show();
            $("#nbrClickspop").text(partie.compteur + ' clics');
            $("#nbrTpspop").text(getTempEnSecond() + ' secondes');
            $("#nbrPtspop").text(partie.score_flag + ' points');
            break;
        case 2:
            $('#abandon_pop').show();
            $('#result_pop').hide();
            console.log($('#result_pop'));
            break;

        default:
            break;
    }
    $('#zoneGrise').delay(10).slideDown().delay(1000).slideUp();
    $('#affichageScore').delay(10).slideDown().delay(1000).slideUp();
}
/*******************ABANDON**************************** */
function skip() {
    affichage(2);
    if (partie.level < FLAGS.length - 1) {
        setTimeout(nextLevel, 2000);
    } else {
        partie.end = true;
        setTimeout(popUpFinal, 2000);
    }
    console.log("ABANDON CLICKED");

}
/*******************RESTART**************************** */


function restartGame() {
    location.reload();
}

/*******************SCORE**************************** */
function getScore() {
    let temps = getTempEnSecond();
    if (FLAGS.nbClick == FLAGS[partie.level].nbClick) {
        if (temps < 5) {
            partie.score_flag += 3;
        } else if (temps > 4 && temps < 10) {
            partie.score_flag += 2;
        } else {
            partie.score_flag += 1;
        }
    } else {
        if (temps < 10) {
            partie.score_flag += 2;
        } else {
            partie.score_flag += 1;
        }
    }
    if (partie.joker) {
        partie.score_flag -= 1;
    }
    partie.score += partie.score_flag;
}

function getTempEnSecond() {
    return partie.timer.h * 3600 + partie.timer.min * 60 + partie.timer.sec;
}

/*******************FLAGS**************************** */
let FLAGS = [{
        name: 'FRANCE',
        colors: ['blue', 'white', 'red'],
        nbClick: 6,
        type: 'vertical_3'
    },
    {
        name: 'BELGIQUE',
        colors: ['black', 'yellow', 'red'],
        nbClick: 6,
        type: 'vertical_3'
    },
    {
        name: 'ALLEMAGNE',
        colors: ['black', 'red', 'yellow'],
        nbClick: 6,
        type: 'horizontal_3'
    },
    {
        name: 'HOLLAND',
        colors: ['red', 'white', 'blue'],
        nbClick: 6,
        type: 'horizontal_3'
    },
    {
        name: 'POLOGNE',
        colors: ['white', 'red'],
        nbClic: 3,
        type: 'horizontal_2'
    },
    {
        name: 'TCHEQUE',
        colors: ['white', 'blue', 'red'],
        nbClick: 6,
        type: 'triangle'
    }
]